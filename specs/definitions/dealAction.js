module.exports = [{
    name: 'dealAction',
    properties: {
        propertyId: {
            type: 'string'
        },
        status: {
            type: 'string'
        },
        dealPrice: {
            type: 'string'
        },
        buyerId: {
            type: 'string'
        }
    }
}]