module.exports = [{
    name: 'searchProperty',
    properties: {
        search: {
            type: 'string'
        },
        searchType: {
            type: 'string'
        },
        minBedrooms: {
            type: 'string'
        },
        maxBedrooms: {
            type: 'string'
        },
        minPrice: {
            type: 'string'
        },
        maxPrice: {
            type: 'string'
        },
        lattitude: {
            type: 'string'
        },
        longitude: {
            type: 'string'
        },
        zipCode: {
            type: 'string'
        },
        miles: {
            type: 'string'
        },
        propertyfor: {
            type: 'string'
        }
    }
}]