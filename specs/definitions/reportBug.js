module.exports = [{
    name: 'reportbug',
    properties: {
        userId: {
            type: 'string'
        },
        bug: {
            type: 'string'
        },
    }
}]