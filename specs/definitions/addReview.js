module.exports = [{
    name: 'addReview',
    properties: {
        userId: {
            type: 'string'
        },
        propertyId: {
            type: 'string'
        },
        review: {
            type: 'string'
        },
        ratings: {
            type: 'string'
        },
    }
}]