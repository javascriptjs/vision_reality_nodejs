module.exports = [{
    name: "updateUser",
    properties: {
        userName: {
            type: "string"
        },
        phoneNumber: {
            type: 'string'
        },
        addressLine1: {
            type: 'string'
        },
        addressLine2: {
            type: 'string'
        },
        email: {
            type: "string"
        },
    }
}];