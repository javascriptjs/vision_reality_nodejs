module.exports = [{
    name: 'homelisting',
    properties: {
        propertyfor: {
            type: 'string'
        },
        lat: {
            type: 'string'
        },
        lng: {
            type: 'string'
        }
    }
}]