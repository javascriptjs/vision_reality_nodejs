module.exports = [

    {
        name: "propertyCreate",
        properties: {
            propertyfor: {
                type: "string"
            },
            description: {
                type: "string"
            },
            closeby: {
                type: "string"
            },
            companyName: {
                type: "string"
            },
            phoneNumber: {
                type: "string"
            },
            email: {
                type: "string"
            },
            propertyName: {
                type: "string"
            },
            propertyCountry: {
                type: "string"
            },
            propertyType: {
                type: "string"
            },
            bedrooms: {
                type: "string"
            },
            bathrooms: {
                type: "string"
            },
            publicTransport: {
                type: "string"
            },
            carParking: {
                type: "string"
            },
            otherInfo: {
                type: "string"
            },
            lattitude: {
                type: "string"
            },
            longitude: {
                type: "string"
            },
            propertyBy: {
                type: "string"
            },
            addressLine1: {
                type: "string"
            },
            addressLine2: {
                type: "string"
            },
            price: {
                type: "string"
            },
            miles: {
                type: "string"
            },
            zipCode: {
                type: "string"
            },
        }
    }
];