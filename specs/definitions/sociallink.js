module.exports = [{
    name: "socialLink",
    properties: {
        socialLinkId: {
            type: "string"
        },
        platform: {
            type: "string"
        },
        email: {
            type: "string"
        },
        userName: {
            type: "string"
        },
        firstName: {
            type: "string"
        },
        lastName: {
            type: "string"
        },
        avatar: {
            type: "string"
        },
        phoneNumber: {
            type: "string"
        },
        deviceToken: {
            type: "string"
        },
        long: {
            type: "string"
        },
        lat: {
            type: "string"
        }
    }
}];