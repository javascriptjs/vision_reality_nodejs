module.exports = [{
    name: "sendMail",
    properties: {
        name: {
            type: "string"
        },
        email: {
            type: "string"
        },
        message: {
            type: "string"
        },
    }
}];