module.exports = [{
    url: "/create",
    post: {
        summary: "create",
        description: "report bug",
        parameters: [{ in: "body",
            name: "body",
            description: "Model of report bug",
            required: true,
            schema: {
                $ref: "#/definitions/reportbug"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
}
]