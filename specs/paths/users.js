module.exports = [{
        url: "/create",
        post: {
            summary: "create",
            description: "create",
            parameters: [{ in: "body",
                name: "body",
                description: "Model of user creation",
                required: true,
                schema: {
                    $ref: "#/definitions/userCreate"
                }
            }],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/login",
        post: {
            summary: "login",
            description: "login",
            parameters: [{ in: "body",
                name: "body",
                description: "Model of user login",
                required: true,
                schema: {
                    $ref: "#/definitions/login"
                }
            }],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/changePassword/{id}",
        put: {
            summary: "Change Password",
            description: "reset Password",
            parameters: [{ in: "path",
                    type: "string",
                    name: "id",
                    description: "user id",
                    required: true
                },
                { in: "body",
                    name: "body",
                    description: "Model of changePassword user",
                    required: true,
                    schema: {
                        $ref: "#/definitions/resetPassword"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: '/forgotPassword',
        post: {
            summary: 'verify email',
            description: 'when user forgot password verify registered email',
            parameters: [{ in: 'body',
                name: 'body',
                description: 'Model of verify registerd email for forgot password',
                required: true,
                schema: {
                    $ref: '#/definitions/forgotPassword'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },

    {
        url: "/newPassword/{id}",
        put: {
            summary: "New Password",
            description: "Set Password",
            parameters: [{ in: "path",
                    type: "string",
                    name: "id",
                    description: "user id",
                    required: true
                },
                { in: "body",
                    name: "body",
                    description: "Model of newPassword user",
                    required: true,
                    schema: {
                        $ref: "#/definitions/newPassword"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/verifyOtp/{id}",
        put: {
            summary: "Verify Otp",
            description: "Verify Otp",
            parameters: [{ in: "path",
                    type: "string",
                    name: "id",
                    description: "user id",
                    required: true
                },
                { in: "body",
                    name: "body",
                    description: "Model of verifyOtp",
                    required: true,
                    schema: {
                        $ref: "#/definitions/verifyOtp"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/getUsers",
        get: {
            summary: "getUsers",
            description: "Just hit the api without pass any param",
            parameters: [{ in: "header",
                name: "x-access-token",
                description: "token to access api",
                required: true,
                type: "string"
            }],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/currentUser/{id}",
        get: {
            summary: "currentUser",
            description: "currentUser",
            parameters: [{ in: "path",
                type: "string",
                name: "id",
                description: "user id",
                required: true
            }, ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/delete/{id}",
        delete: {
            summary: "delete",
            description: "delete",
            parameters: [{ in: "path",
                type: "string",
                name: "id",
                description: "user id",
                required: true
            }, ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/update/{id}",
        put: {
            summary: "update",
            description: "update",
            parameters: [{ in: "path",
                    type: "string",
                    name: "id",
                    description: "user id",
                    required: true
                },
                { in: "body",
                    name: "body",
                    description: "Model of user login",
                    required: true,
                    schema: {
                        $ref: "#/definitions/updateUser"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/uploadProfilePic/{id}",
        put: {
            summary: "upload Profile Pic ",
            description: "upload Profile Pic ",
            parameters: [{ in: "formData",
                    name: "image",
                    type: "file",
                    description: "The file to upload.",
                    required: true,
                },
                { in: "path",
                    type: "string",
                    name: "id",
                    description: "user id",
                    required: true
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/socialLink",
        post: {
            summary: "Login user with socialLink",
            description: "user login into system with socialLink and get its token to access apis",
            parameters: [{ in: "body",
                name: "body",
                description: "Model of login user with socialLink",
                required: true,
                schema: {
                    $ref: "#/definitions/socialLink"
                }
            }],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/sendMail",
        post: {
            summary: "sendMail",
            description: "sendMail",
            parameters: [{ in: "body",
                name: "body",
                description: "sendMail",
                required: true,
                schema: {
                    $ref: "#/definitions/sendMail"
                }
            }],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: '/searchUser',
        post: {
            summary: 'Search Users',
            description: 'Search by userName',
            parameters: [{ in: 'body',
                name: 'body',
                description: 'Model of search user by userName',
                required: true,
                schema: {
                    $ref: '#/definitions/searchUser'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
];