module.exports = [{
    url: '/getOldChat',
    get: {
        summary: 'get old chat',
        description: 'get old chat',
        parameters: [{
            in: 'query',
            type: "string",
            name: 'room_id',
            description: 'Room Id',
            required: true,
        }, {
            in: 'query',
            type: "integer",
            name: 'pageNo',
            description: 'pageNo',
            required: true,
        }, {
            in: 'query',
            type: "integer",
            name: 'pageSize',
            description: 'pageSize',
            required: true,
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
},
{
    url: '/recentChats',
    get: {
        summary: 'get recent chats',
        description: 'get recent chats',
        parameters: [{
            in: 'query',
            type: "string",
            name: 'userName',
            description: 'Enter Username of User',
            required: true,
        }, {
            in: 'query',
            type: "integer",
            name: 'pageNo',
            description: 'pageNo',
            required: true,
        }, {
            in: 'query',
            type: "integer",
            name: 'pageSize',
            description: 'pageSize',
            required: true,
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
},
{
    url: '/searchChats',
    get: {
        summary: 'get recent chats',
        description: 'get recent chats',
        parameters: [{
            in: 'query',
            type: "string",
            name: 'search',
            description: 'Enter Username of User',
            required: true,
        }, 
        // {
        //     in: 'query',
        //     type: "integer",
        //     name: 'pageNo',
        //     description: 'pageNo',
        //     required: true,
        // }, {
        //     in: 'query',
        //     type: "integer",
        //     name: 'pageSize',
        //     description: 'pageSize',
        //     required: true,
        // }
        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
}

]
