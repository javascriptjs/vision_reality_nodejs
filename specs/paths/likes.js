module.exports = [{
    url: "/create",
    post: {
        summary: "create",
        description: "likeCount",
        parameters: [{ in: "body",
            name: "body",
            description: "Model of create brand",
            required: true,
            schema: {
                $ref: "#/definitions/likeCount"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/getFavProperties",
    get: {
        summary: "getFavProperties",
        description: "Just hit the api",
        parameters: [{ in: "header",
            name: "x-access-token",
            description: "token to access api",
            required: false,
            type: "string"
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
// {
//     url: "/byUser/{id}",
//     get: {
//         summary: "getbyUser",
//         description: "getbyUser",
//         parameters: [{ in: "path",
//             type: "string",
//             name: "id",
//             description: "User id",
//             required: true
//         }, ],
//         responses: {
//             default: {
//                 description: "Unexpected error",
//                 schema: {
//                     $ref: "#/definitions/Error"
//                 }
//             }
//         }
//     }
// },
]