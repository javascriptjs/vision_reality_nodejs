module.exports = [{
    url: "/create",
    post: {
        summary: "create",
        description: "create",
        parameters: [{ in: "header",
            name: "x-access-token",
            description: "token to access api",
            required: true,
            type: "string"
        },
        { in: "body",
            name: "body",
            description: "Model of property creation",
            required: true,
            schema: {
                $ref: "#/definitions/propertyCreate"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
// {
//     url: "/uploadPropertyFiles/{id}",
//     put: {
//         summary: "Upload Property Images",
//         description: "Upload Property Images ",
//         parameters: [
//             { in: "path",
//                 type: "string",
//                 name: "id",
//                 description: "Property ID",
//                 required: true
//             },
//             { in: "formData",
//                 name: "bathroomFiles[]",
//                 type: "file",
//                 description: "The file to upload.",
//                 required: false,
//             },
//             { in: "formData",
//                 name: "washroomFiles[]",
//                 type: "file",
//                 description: "The file to upload.",
//                 required: false,
//             },
//             { in: "formData",
//                 name: "internalFiles[]",
//                 type: "file",
//                 description: "The file to upload.",
//                 required: false,
//             },
//             { in: "formData",
//                 name: "externalFiles[]",
//                 type: "file",
//                 description: "The file to upload.",
//                 required: false,
//             },{ in: "formData",
//                 name: "otherFiles[]",
//                 type: "file",
//                 description: "The file to upload.",
//                 required: false,
//             },
//         ],
//         responses: {
//             default: {
//                 description: "Unexpected error",
//                 schema: {
//                     $ref: "#/definitions/Error"
//                 }
//             }
//         }
//     }
// },
{
    url: "/uploadPropertyFiles/{id}",
    put: {
        summary: "Upload Property Files",
        description: "Upload Property Files ",
        parameters: [{ in: "formData",
                name: "files[]",
                type: "file",
                description: "The file to upload.",
                required: true,
            },
            { in: "path",
                type: "string",
                name: "id",
                description: "Property Id",
                required: true
            }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/propertyDetails/{id}",
    get: {
        summary: "propertyDetails",
        description: "propertyDetails",
        parameters: [{ 
            in: "header",
            name: "x-access-token",
            description: "token to access api",
            required: true,
            type: "string"
        },
        { 
            in: "path",
            type: "string",
            name: "id",
            description: "Porperty Id",
            required: true
        },],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/homeListing",
    post: {
        summary: "homeListing",
        description: "Just hit the api without pass any param",
        parameters: [
            { in: "header",
            name: "x-access-token",
            description: "token to access api",
            required: false,
            type: "string"
        },
        { in: "body",
            name: "body",
            description: "Model of homelisting",
            required: true,
            schema: {
                $ref: "#/definitions/homelisting"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},

{
    url: '/searchProperty',
    post: {
        summary: 'Search Properties',
        description: 'Search by property name',
        parameters: [{ in: 'body',
            name: 'body',
            description: 'Model of search property by name',
            required: true,
            schema: {
                $ref: '#/definitions/searchProperty'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
},

{
    url: "/myProperty",
    get: {
        summary: "myProperty",
        description: "Just hit the api without pass any param",
        parameters: [{ in: "header",
            name: "x-access-token",
            description: "token to access api",
            required: true,
            type: "string"
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},

{
    url: "/delete/{id}",
    delete: {
        summary: "delete",
        description: "delete",
        parameters: [{ in: "path",
            type: "string",
            name: "id",
            description: "Property id",
            required: true
        }, ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/createDeal/{id}",
    post: {
        summary: "createDeal",
        description: "createDeal",
        parameters: [
            { in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
            },
            { in: "path",
                type: "string",
                name: "id",
                description: "property id",
                 required: true
            },
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/dealAction",
    post: {
        summary: "dealAction",
        description: "dealAction",
        parameters: [
            { in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
            },
            { in: "body",
                    name: "body",
                    description: "Model of newPassword user",
                    required: true,
                    schema: {
                        $ref: "#/definitions/dealAction"
                    }
            }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/activeDeals",
    get: {
        summary: "activeDeals",
        description: "Just hit the api without pass any param",
        parameters: [{ in: "header",
            name: "x-access-token",
            description: "token to access api",
            required: true,
            type: "string"
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/doneDeals",
    get: {
        summary: "doneDeals",
        description: "Just hit the api without pass any param",
        parameters: [{ in: "header",
            name: "x-access-token",
            description: "token to access api",
            required: true,
            type: "string"
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/ownDeals",
    get: {
        summary: "ownDeals",
        description: "Just hit the api without pass any param",
        parameters: [{ in: "header",
            name: "x-access-token",
            description: "token to access api",
            required: true,
            type: "string"
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
];