'use strict'

exports.toModel = (entity) => {
    const model = {
        fromMsg: entity.msgFrom,
        toMsg: entity.msgTo,
        msg: entity.msg,
        readStatus: entity.readStatus,
        avatar: entity.avatar,
        date: entity.createdOn,
    }
    return model
}

exports.toSearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toModel(entity)
    })
}
