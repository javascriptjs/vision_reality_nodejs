'use strict'

exports.toModel = (entity) => {
    const model = {
        room_id: entity._id,
        // fromMsg: entity.msgFrom,
        // toMsg: entity.msgTo,
        // msg: entity.msg,
        // date: entity.createdOn,
        // name1: entity.msgFrom,
        // name2: entity.msgFrom,
        sender: entity.sender,
        reciever: entity.reciever,
        lastSeen: entity.lastActive
    }
    return model
}

exports.toSearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toModel(entity)
    })
}
