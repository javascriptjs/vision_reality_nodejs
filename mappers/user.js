"use strict";

exports.toModel = entity => {
    const model = {
        _id: entity._id,
        userName: entity.userName,
        firstName: entity.firstName,
        lastName: entity.lastName,
        phoneNumber: entity.phoneNumber,
        socialLinkId: entity.socialLinkId,
        platform: entity.platform,
        email: entity.email,
        isOnBoardingDone: entity.isOnBoardingDone,
        addressLine1: entity.addressLine1,
        addressLine2: entity.addressLine2,
        status: entity.status,
        sex: entity.sex,
        city: entity.city,
        country: entity.country,
        zipCode: entity.zipCode,
        lat: entity.lat,
        long: entity.long,
        stripeToken: entity.stripeToken,
        stripeKey: entity.stripeKey,
        ssn: entity.ssn,
        isActivated: entity.isActivated,
        lastModifiedBy: entity.lastModifiedBy,
        createdBy: entity.createdBy,
        deviceToken: entity.deviceToken,
        role: entity.role,
        apiToken: entity.apiToken,
        updatedOn: entity.updatedOn,
        createdOn: entity.createdOn,
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map(entity => {
        return exports.toModel(entity);
    });
};

exports.toLoginModel = entity => {
    let login = exports.toModel(entity);
    login.token = entity.token
    return login
};