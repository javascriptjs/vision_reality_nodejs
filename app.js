"use strict";

const express = require("express");
const appConfig = require("config").get("app");
const service = require("./services/userService");
const logger = require("@open-age/logger")("server");
const Http = require("http");
const path = require('path');
const port = process.env.PORT || appConfig.port || 3000;
var admin = require("firebase-admin");
var serviceAccount = require("./firebase-truckapp.json");
const app = express();
var bodyParser = require('body-parser');
const bcrypt = require("bcrypt");
var server = Http.createServer(app);

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

require('./communication/chat.js').sockets(server);


app.get('/resetPassword/:token', function (req, res) {
    res.sendFile(path.join(__dirname + '/email_templates/index.html'));
})

app.post("/resetPassword/:token", async function (req, res) {
    const { Password } = req.body;
    if (!Password) {
        return res.status(400).json({
            status: false,
            message: "Password is required!",
        });
    } else {

        async function resetPassword(user) {
            user.password = bcrypt.hashSync(Password, 10);
            user.updatedOn = new Date();
            await user.save()
            return res.json({
                status: 200,
                message: "Password changed successfully",
            });
        }
        db.user.findOne({
            resetPasswordToken: req.params.token,
            resetPasswordExpires: { $gt: Date.now() },
        }).then((user) => {
            if (!user) {
                throw new Error("otpVerifyToken is wrong or expired.");
            }
            resetPassword(user);
        })
            .catch((err) => {
                return res.json({
                    status: "error",
                    message: err.message,
                });
            })
    };
});

const boot = () => {
    const log = logger.start("app:boot");
    log.info(`environment:  ${process.env.NODE_ENV}`);
    log.info("starting server");
    server.listen(port, () => {
        log.info(`listening on port: ${port}`);
        log.end();
    });
};

const init = async () => {
    await require("./settings/database").configure(logger);
    await require("./settings/express").configure(app, logger);
    await require("./settings/routes").configure(app, logger);
    app.get('/chat', function (req, res) {
        res.sendFile(__dirname + '/templates/index.html');
      });

    app.get('/privacy', function(req, res) {
        res.sendFile(__dirname + '/templates/privacy.html'); // replace /public with your directory
    });
    
    app.get('/terms', function(req, res) {
        res.sendFile(__dirname + '/templates/terms.html'); // replace /public with your directory
    });
    boot();
};
init();