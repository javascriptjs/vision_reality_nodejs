"use strict";
const path = require("path");
const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");
var multer = require('multer');

var baseDir = process.cwd();
var uploadPath = baseDir +"/uploads/";
var upload = multer({dest: uploadPath });

const configure = async (app, logger) => {
  const log = logger.start("settings:express:configure");
  app.use(bodyParser.json({limit: "50mb"}));
  app.use(cors());
  //app.use(bodyParser.json({limit: "50mb"}));
//app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
  app.use(
    bodyParser.urlencoded({
      extended: true,
      limit: "50mb", parameterLimit:50000
    })
  );

  app.use(
    bodyParser({
      limit: "50mb",
      keepExtensions: true
    })
  );

//   app.use(multer({
//       dest: baseDir +"/uploads/",
//         rename: function (fieldname, filename) {
//           console.log(filename + Date.now());
//           return filename + Date.now();
//         },
//         onFileUploadStart: function (file) {
//           console.log(file.originalname + ' is starting ...');
//         },
//         onFileUploadComplete: function (file) {
//           console.log(file.fieldname + ' uploaded to  ' + file.path);
//         }
// }).single('image'));

  const root = path.normalize(__dirname + "./../");
  app.use(express.static(path.join(root, "public")));

  log.end();
};

exports.configure = configure;
