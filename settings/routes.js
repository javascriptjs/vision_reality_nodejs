"use strict";

const fs = require("fs");
const api = require("../api");
const specs = require("../specs");
const permit = require("../permit")
const path = require("path");
const validator = require("../validators");
var multer = require('multer');

var storage = multer.diskStorage({

    destination: function(req, file, cb) {
        if (file.fieldname == 'csv') {
            cb(null, path.join(__dirname, '../', 'assets'));
        } else {
            cb(null, path.join(__dirname, '../', 'assets/images'));
        }
    },
    filename: function(req, file, cb) {
        if (file.fieldname == 'csv') {
            cb(null, file.originalname);
        } else {
            cb(null, Date.now() + file.originalname);
        }
    }
});

var upload = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });

const configure = (app, logger) => {
    const log = logger.start("settings:routes:configure");
    app.get("/specs", function(req, res) {
        fs.readFile("./public/specs.html", function(err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                });
            }
            res.contentType("text/html");
            res.send(data);
        });
    });
    app.get("/api/specs", function(req, res) {
        res.contentType("application/json");
        res.send(specs.get());
    });

    app.post(
        "/api/users/create",
        permit.context.builder,
        validator.users.create,
        api.users.create
    );

    app.post(
        "/api/users/login",
        permit.context.builder,
        validator.users.login,
        api.users.login
    );

    app.post(
        "/api/users/socialLink",
        permit.context.builder,
        api.users.socialLink
    );

    app.get(
        "/api/users/currentUser/:id",
        permit.context.builder,
        api.users.currentUser
    );

    app.put(
        "/api/users/changePassword/:id",
        permit.context.builder,
        api.users.changePassword,
        validator.users.changePassword,
    );

    app.get(
        "/api/users/addDetails",
        permit.context.builder,
        api.users.addDetails
    );

    app.post(
        "/api/users/forgotPassword",
        permit.context.builder,
        api.users.forgotPassword
    );

    app.put(
        "/api/users/newPassword/:id",
        permit.context.builder,
        api.users.newPassword,
    );

    app.put(
        "/api/users/verifyOtp/:id",
        permit.context.builder,
        api.users.verifyOtp
    );

    app.get(
        "/api/users/getUsers",
        permit.context.requiresToken,
        api.users.getUsers
    );

    app.delete(
        "/api/users/delete/:id",
        permit.context.builder,
        api.users.deleteUser
    );

    app.put(
        "/api/users/update/:id",
        permit.context.builder,
        validator.users.update,
        api.users.update
    );

    app.put(
        "/api/users/uploadProfilePic/:id",
        permit.context.builder,
        upload.single('image'),
        api.users.uploadProfilePic
    );

    app.put(
        '/api/users/image/:uploadImageOf/:id',
        permit.context.builder,
        api.users.imageUploader
    )

    app.post(
        "/api/users/searchUser",
        permit.context.builder,
        api.users.searchUser
    );

    app.post(
        "/api/users/sendMail",
        permit.context.builder,
        api.users.sendMail
    );
    /* PROPERTY ROUTES */

    app.post(
        "/api/property/create",
        permit.context.requiresToken,
        validator.property.create,
        api.property.create
    );

    // app.put(
    //     "/api/property/uploadPropertyFiles/:id",
    //     permit.context.builder,
    //     upload.any(),
    //     api.property.uploadPropertyFiles
    // );

    app.put(
        "/api/property/uploadPropertyFiles/:id",
        permit.context.builder,
        upload.array('files[]'),
        api.property.uploadPropertyFiles
    );

    app.get(
        "/api/property/propertyDetails/:id",
        permit.context.userToken,
        api.property.propertyDetails
    );

    app.post(
        "/api/property/homeListing",
        permit.context.userToken,
        api.property.homeListing
    );

    app.post(
        "/api/property/searchProperty",
        permit.context.builder,
        api.property.searchProperty
    );

    app.get(
        "/api/property/myProperty",
        permit.context.builder,
        api.property.myProperty
    );

    app.delete(
        "/api/property/delete/:id",
        permit.context.builder,
        api.property.deleteProperty
    );

    app.post(
        "/api/property/createDeal/:id",
        permit.context.userToken,
        api.property.createDeal
    );

    app.post(
        "/api/property/dealAction",
        permit.context.userToken,
        api.property.dealAction
    );

    app.get(
        "/api/property/activeDeals",
        permit.context.userToken,
        api.property.activeDeals
    );

    app.get(
        "/api/property/doneDeals",
        permit.context.userToken,
        api.property.doneDeals
    );

    app.get(
        "/api/property/ownDeals",
        permit.context.userToken,
        api.property.ownDeals
    );

    app.post(
        "/api/likes/create",
        permit.context.builder,
        api.likes.create
    );

    app.get(
        "/api/likes/getFavProperties",
        permit.context.userToken,
        api.likes.getFavProperties
    );

    app.post(
        "/api/reviews/create",
        permit.context.builder,
        api.reviews.create
    );

    //// consversations ///

    app.get(
        '/api/conversations/getOldChat',
        permit.context.builder,
        api.conversations.getOldChat
    )

    app.get(
        "/api/conversations/recentChats",
        permit.context.builder,
        api.conversations.recentChats
    );

    app.get(
        "/api/conversations/searchChats",
        permit.context.builder,
        api.conversations.searchChats
    );

    app.post(
        "/api/reportbug/create",
        permit.context.builder,
        api.reportbug.create
    );

    log.end();
};

exports.configure = configure;