"use strict";
const response = require("../exchange/response");

const addDetails = (req, res, next) => {
  const log = req.context.logger.start("validators:users:addDetails");

  if (!req.body) {
    log.end();
    return response.failure(res, "body is equired");
  }
  // if (!req.body.quantity) {
  //   log.end();
  //   return response.failure(res, "quantity is required");
  // }
  // if (!req.body.) {
  //   log.end();
  //   return response.failure(res, "ratio is required");
  // }
  

  log.end();
  return next();
};

exports.addDetails = addDetails;

