"use strict";
const response = require("../exchange/response");

const create = (req, res, next) => {
    const log = req.context.logger.start("validators:property:create");

    if (!req.body) {
        log.end();
        return response.failure(res, "body is equired");
    }
    /*if (!req.body.email) {
        log.end();
        return response.failure(res, "email is required");
    }
    if (!req.body.companyName) {
        log.end();
        return response.failure(res, "Company Name is required");
    }
    if (!req.body.phoneNumber) {
        log.end();
        return response.failure(res, "Phone Number is required");
    }
    if (!req.body.propertyCountry) {
        log.end();
        return response.failure(res, "Property Country is required");
    }*/
    log.end();
    return next();
};

exports.create = create;