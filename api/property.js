"use strict";
const service = require("../services/propertyService");
const response = require("../exchange/response");
const encrypt = require("../permit/crypto.js");


//register api

const create = async(req, res) => {
    const log = req.context.logger.start(`api:property:create`);

    try {
        const property = await service.create(req.body, req.context);
        const message = "Property Added Successfully";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


//Property Details
const propertyDetails = async(req, res) => {
    const log = req.context.logger.start(`api:property:propertyDetails`);
    try {
        const property = await service.propertyDetails(req.params.id, req.body, req.context);
        const message = "Property Details";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


// //homeListing
const homeListing = async(req, res) => {
    const log = req.context.logger.start(`api:property:homeListing`);
    try {
        const property = await service.homeListing(req.body, req.context);
        const message = "HomeListing fetched Successfully";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const myProperty = async(req, res) => {
    const log = req.context.logger.start(`api:property:myProperty`);
    try {
        const property = await service.myProperty(req.body, req.context);
        const message = "My Properties fetched Successfully";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// //searchProperty
const searchProperty = async(req, res) => {
    const log = req.context.logger.start(`api:property:searchProperty`);
    try {
        const property = await service.searchProperty(req.body, req.context);
        const message = "Properties fetched Successfully";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// const deleteUser = async(req, res) => {
//     const log = req.context.logger.start(`api:users:deleteUser`);
//     try {
//         const user = await service.deleteUser(req.params.id, req.context);
//         log.end();
//         return response.data(res, user);
//     } catch (err) {
//         log.error(err);
//         log.end();
//         return response.failure(res, err.message);
//     }
// };

// const update = async(req, res) => {
//     const log = req.context.logger.start(`api:users:update`);
//     try {
//         const user = await service.update(req.params.id, req.body, req.context);
//         log.end();
//         return response.data(res, userMapper.toModel(user));
//     } catch (err) {
//         log.error(err);
//         log.end();
//         return response.failure(res, err.message);
//     }
// };

const deleteProperty = async(req, res) => {
    const log = req.context.logger.start(`api:property:deleteProperty`);
    try {
        const property = await service.deleteProperty(req.params.id, req.context);
        const message = property.message
        log.end();
        return response.message(res, message);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const createDeal = async(req, res) => {
    const log = req.context.logger.start(`api:property:createDeal`);

    try {
        const property = await service.createDeal(req.params.id,req.body, req.context);
        const message = "Deal Created Successfully";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const dealAction = async(req, res) => {
    const log = req.context.logger.start(`api:property:dealAction`);

    try {
        const property = await service.dealAction(req.body, req.context);
        const message = "Deal Created Successfully";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const activeDeals = async(req, res) => {
    const log = req.context.logger.start(`api:property:activeDeals`);
    try {
        const property = await service.activeDeals(req.body, req.context);
        const message = "Active Deals fetched Successfully";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const doneDeals = async(req, res) => {
    const log = req.context.logger.start(`api:property:doneDeals`);
    try {
        const property = await service.doneDeals(req.body, req.context);
        const message = "Done Deals fetched Successfully";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const ownDeals = async(req, res) => {
    const log = req.context.logger.start(`api:property:ownDeals`);
    try {
        const property = await service.ownDeals(req.body, req.context);
        const message = "Own Deals fetched Successfully";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const uploadPropertyFiles = async(req, res) => {
    const log = req.context.logger.start(`api:property:uploadPropertyFiles`);
    try {
        const property = await service.uploadPropertyFiles(req.params.id, req.files,req.body, req.context);
        const message = "Property Updated Successfully!!";
        log.end();
        return response.success(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }

};

// const uploadPropertyFiles = async(req, res) => {
//     const log = req.context.logger.start(`api:property:uploadPropertyFiles`);
//     try {
//         const property = await service.uploadPropertyFiles(req.params.id, req.files,req.body, req.context);
//         const message = "Property Updated Successfully!!";
//         log.end();
//         return response.success(res, message, property);
//     } catch (err) {
//         log.error(err);
//         log.end();
//         return response.failure(res, err.message);
//     }

// };


// const imageUploader = async(req, res) => {
//     let log = req.context.logger.start(`api:users:imageUploader`);
//     console.log('api hit image upload===');
//     try {
//         const responseData = await service.imageUploader(req, req.params, req.context)
//         res.message = 'File Uploaded Successfully.'
//         log.end()
//         return response.data(res, responseData)
//     } catch (err) {
//         log.error(err)
//         log.end()
//         response.failure(res, err.message)
//     }
// }

exports.create = create;
exports.propertyDetails = propertyDetails;
exports.homeListing = homeListing;
exports.searchProperty = searchProperty;
exports.myProperty = myProperty;
exports.createDeal = createDeal;
exports.deleteProperty = deleteProperty;
exports.dealAction = dealAction;
exports.uploadPropertyFiles = uploadPropertyFiles;
exports.activeDeals = activeDeals;
exports.doneDeals = doneDeals;
exports.ownDeals = ownDeals;