const service = require("../services/likes");
const response = require("../exchange/response");

const create = async(req, res) => {
    const log = req.context.logger.start(`api:likes:create`);
    try {
        const likeRes = await service.create(req.body, req.context);
        if (likeRes.err === null || likeRes.err === undefined) {
            if (likeRes._id) {
                const message = "Like Property Successfully!!";
                log.end();
                return response.success(res, message, likeRes);
            } else {
                const message = "DisLike Property Successfully!!"
                log.end();
                return response.success(res, message, likeRes);
            }
        } else {
            log.end();
            return response.failure(res, likeRes.err);
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const getFavProperties = async(req, res) => {
    const log = req.context.logger.start(`api:likes:getFavProperties`);
    try {
        const property = await service.getFavProperties(req.params.id, req.body, req.context);
        const message = "Get Favorite Properties";
        log.end();
        return response.getData(res, message, property);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// const byUser = async(req, res) => {
//     const log = req.context.logger.start(`api:likes:byUser`);
//     try {
//         const product = await service.byUser(req.params.id, req.body, req.context);
//         const message = "Get Products";
//         log.end();
//         return response.getData(res, message, product);
//     } catch (err) {
//         log.error(err);
//         log.end();
//         return response.failure(res, err.message);
//     }
// };

exports.create = create;
exports.getFavProperties = getFavProperties;
// exports.byUser = byUser;