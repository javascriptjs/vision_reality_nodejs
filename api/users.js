"use strict";
const service = require("../services/userService");
const response = require("../exchange/response");
const encrypt = require("../permit/crypto.js");
const userMapper = require("../mappers/user");


//register api

const create = async(req, res) => {
    const log = req.context.logger.start(`api:users:create`);

    try {
        const user = await service.create(req.body, req.context);
        const message = "User Register Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

//login api  

const login = async(req, res) => {
    const log = req.context.logger.start("api:users:login");
    try {
        const user = await service.login(req.body, req.context);
        log.end();
        return response.authorized(res, user, user.token);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const socialLink = async(req, res) => {
    const log = req.context.logger.start("api:users:socialLink");
    try {
        const user = await service.socialLink(req.body, req.context);
        log.end();
        return response.authorized(res, userMapper.toLoginModel(user), user.token);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};



// adddetails

const addDetails = async(req, res) => {
    const log = req.context.logger.start(`api:users:addDetails`);

    try {
        const user = await service.addDetails(req.body, req.context);
        const message = "Details added Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// current user
const currentUser = async(req, res) => {
    const log = req.context.logger.start(`api:users:currentUser`);
    try {
        const user = await service.currentUser(req.params.id, req.body, req.context);
        const message = "Current User";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

//New Password

const newPassword = async(req, res) => {
    const log = req.context.logger.start("api:users:newPassword");
    try {
        const message = await service.newPassword(req.params.id, req.body, req.context);
        log.end();
        return response.success(res, message);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// change password

const changePassword = async(req, res) => {
    const log = req.context.logger.start("api:users:changePassword");
    try {
        const message = await service.changePassword(req.params.id, req.body, req.context);
        log.end();
        return response.success(res, message);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// FORGET PASSWORD

const forgotPassword = async(req, res) => {
    const log = req.context.logger.start(`api:users:forgotPassword`);
    try {
        const result = await service.forgotPassword(req.body, req.context);
        const message = "Password reset email is sent to your registerd email";
        log.end();
        return response.success(res, message, result);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const sendMail = async(req, res) => {
    const log = req.context.logger.start(`api:users:sendMail`);
    try {
        const result = await service.sendMail(req.body, req.context);
        const message = "Mail Sent";
        log.end();
        return response.success(res, message, result);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

//verifyOtp

const verifyOtp = async(req, res) => {
    const log = req.context.logger.start("api:users:verifyOtp");
    try {
        const user = await service.verifyOtp(req.params.id, req.body, req.context);
        const message = "OTP Verified"
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

//getUsers
const getUsers = async(req, res) => {
    const log = req.context.logger.start(`api:users:getUsers`);
    try {
        const user = await service.getUsers(req.body, req.context);
        const message = "User get Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const deleteUser = async(req, res) => {
    const log = req.context.logger.start(`api:users:deleteUser`);
    try {
        const user = await service.deleteUser(req.params.id, req.context);
        log.end();
        return response.data(res, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const update = async(req, res) => {
    const log = req.context.logger.start(`api:users:update`);
    try {
        const user = await service.update(req.params.id, req.body, req.context);
        log.end();
        return response.data(res, userMapper.toModel(user));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const uploadProfilePic = async(req, res) => {
    const log = req.context.logger.start(`api:users:uploadProfilePic`);
    try {
        const user = await service.uploadProfilePic(req.params.id, req.file, req.context);
        const message = "Profile Picture Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }

};


const imageUploader = async(req, res) => {
    let log = req.context.logger.start(`api:users:imageUploader`);
    console.log('api hit image upload===');
    try {
        const responseData = await service.imageUploader(req, req.params, req.context)
        res.message = 'File Uploaded Successfully.'
        log.end()
        return response.data(res, responseData)
    } catch (err) {
        log.error(err)
        log.end()
        response.failure(res, err.message)
    }
}

const searchUser = async(req, res) => {
    const log = req.context.logger.start(`api:users:searchUser`);
    try {
        const allusers = await service.searchUser(req.body, req.context);
        const message = "Users fetched Successfully";
        log.end();
        return response.success(res, message, allusers);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.login = login;
exports.create = create;
exports.currentUser = currentUser;
exports.changePassword = changePassword;
exports.verifyOtp = verifyOtp;
exports.newPassword = newPassword;
exports.addDetails = addDetails;
exports.forgotPassword = forgotPassword;
exports.getUsers = getUsers;
exports.deleteUser = deleteUser;
exports.update = update;
exports.uploadProfilePic = uploadProfilePic;
exports.imageUploader = imageUploader;
exports.socialLink = socialLink;
exports.searchUser = searchUser;
exports.sendMail = sendMail;