const service = require("../services/reviews");
const response = require("../exchange/response");

const create = async(req, res) => {
    const log = req.context.logger.start(`api:reviews:create`);
    try {
        const reviewRes = await service.create(req.body, req.context);
        const message = "Review Added Successfully";
        log.end();
        return response.success(res, message, reviewRes);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


// const getFavProperties = async(req, res) => {
//     const log = req.context.logger.start(`api:likes:getFavProperties`);
//     try {
//         const property = await service.getFavProperties(req.params.id, req.body, req.context);
//         const message = "Get Favorite Properties";
//         log.end();
//         return response.getData(res, message, property);
//     } catch (err) {
//         log.error(err);
//         log.end();
//         return response.failure(res, err.message);
//     }
// };

exports.create = create;
// exports.getFavProperties = getFavProperties;