const service = require("../services/reportbug");
const response = require("../exchange/response");

const create = async(req, res) => {
    const log = req.context.logger.start(`api:reportbug:create`);
    try {
        const reviewRes = await service.create(req.body, req.context);
        const message = "Bug Added Successfully";
        log.end();
        return response.success(res, message, reviewRes);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.create = create;