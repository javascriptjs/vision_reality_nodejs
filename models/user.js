"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const user = mongoose.Schema({
    userName: { type: String, default: "", required: true },
    firstName: { type: String, default: "", required: false },
    lastName: { type: String, default: "", required: false },
    email: { type: String, default: "", required: false },
    phoneNumber: { type: String, default: "" },
    password: { type: String, default: "", required: false },
    socialLinkId: { type: String, default: "", required: false },
    socialType: { type: String, default: "", required: false },
    resetPasswordToken: {
        type: String,
        required: false
    },
    resetPasswordExpires: {
        type: Date,
        required: false
    },
    sex: { type: String, default: "" },
    profilePic: { type: String, default: "" },
    pofileCompleted: { type: Number, default: 20, required: true },
    OTP: { type: String, default: "" },
    verifyOTP: { type: Boolean, default: false },
    addressLine1: { type: String, default: "" },
    addressLine2: { type: String, default: "" },
    city: { type: String, default: "" },
    country: { type: String, default: "" },
    zipCode: { type: String, default: "" },
    type: { type: String, default: "" },
    status: { type: String, default: "Offline" },
    avatar: { type: String, default: "http://72.167.44.37/vision_reality_nodejs/assets/images/default.jpg" },
    stripeToken: { type: String, default: "" },
    stripeKey: { type: String, default: "" },
    lastLoggedIn: { type: Date, default: "" },
    personalNote: { type: String, default: "" },
    loginLimit: { type: Number, default: 0 },
    inviteBy: { type: String, default: "" },
    ssn: { type: String, default: "" },
    isActivated: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false },
    lastModifiedBy: { type: String, default: "" },
    deletedBy: { type: String, default: "" },
    apiToken: { type: String, default: "" },
    createdBy: { type: String, default: "" },
    role: { type: String, default: "" },
    deviceToken: { type: String, default: "" },
    lastseen: { type: String, default: "" },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },

});

mongoose.model("user", user);
module.exports = user;