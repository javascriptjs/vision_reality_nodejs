"use strict";
const mongoose = require("mongoose");
const review = mongoose.Schema({
    propertyId: { type: mongoose.Schema.Types.ObjectId, ref: 'property' },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    review: { type: String, default: "", required: true },
    ratings: { type: String, default: "", required: false },
    createdOn: { type: Date, default: Date.now },
});

mongoose.model("review", review);
module.exports = review;