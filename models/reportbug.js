"use strict";
const mongoose = require("mongoose");
const reportbug = mongoose.Schema({
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    bug: { type: String, default: "", required: true },
    createdOn: { type: Date, default: Date.now },
});

mongoose.model("reportbug", reportbug);
module.exports = reportbug;