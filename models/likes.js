"use strict";
const mongoose = require("mongoose");
const like = mongoose.Schema({
    propertyId: { type: mongoose.Schema.Types.ObjectId, ref: 'property' },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
});

mongoose.model("like", like);
module.exports = like;