"use strict";
const mongoose = require("mongoose");
const deal = mongoose.Schema({
    propertyId: { type: mongoose.Schema.Types.ObjectId, ref: 'property' },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    status: { type: String, default: "requested" },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
});

mongoose.model("deal", deal);
module.exports = deal;