'use strict'
// const hooks = require('../hooks/users')
const mongoose = require('mongoose')

const room = mongoose.Schema({
    name1: { type: String, default: "", required: true },
    name2: { type: String, default: "", required: true },
    members: [],
    sender: { type: String, default: "", required: true },
    reciever: { type: String, default: "", required: true },
    avatar: { type: String, default: "", required: false },
    status: { type: String, default: "", required: false },
    lastMessage: { type: String, default: "" },
    lastLoggedIn: { type: Date, default: Date.now },
    lastActive: { type: Date, default: Date.now },
    unreadMsgCount: { type: Number, default: 0 },
    createdOn: { type: Date, default: Date.now }
})

mongoose.model('Room', room);
// hooks.configure(user)
module.exports = room
