'use strict'

const mongoose = require('mongoose')

const chat = mongoose.Schema({
    msgFrom: { type: String, default: "", required: true },
    msgTo: { type: String, default: "", required: true },
    msg: { type: String, default: "", required: true },
    room: { type: String, default: "", required: true },
    avatar: { type: String, default: "", required: false },
    readStatus: { type: String, default: "Unread", required: false },
    createdOn: { type: Date, default: Date.now }
})

mongoose.model('Conversation', chat);
module.exports = chat
