"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const property = mongoose.Schema({
    propertyfor: { type: String, default: "" },
    closeby: { type: String, default: "" },
    propertyBy: { type: String, default: "" },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    description: { type: String, default: "", required: false },
    companyName: { type: String, default: "", required: false },
    phoneNumber: { type: String, default: "" },
    email: { type: String, default: "", required: false },
    propertyName: { type: String, default: "", required: false },
    propertyCountry: { type: String, default: "", required: false },
    state: { type: String, default: "", required: false },
    propertyType: { type: String, default: "" },
    bedrooms: { type: Number, required: false },
    bathrooms: { type: Number, required: false },
    publicTransport: { type: String, default: "" },
    otherInfo: { type: String, default: "" },
    addressLine1: { type: String, default: "" },
    addressLine2: { type: String, default: "" },
    lattitude: { type: String, default: "" },
    longitude: { type: String, default: "" },
    zipCode: { type: String, default: "" },
    type: { type: String, default: "" },
    status: { type: String, default: "active" },
    price: { type: Number, default: 0 },
    dealPrice: { type: String, default: "" },
    buyerId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    miles: { type: String, default: "" },
    carParking: { type: String, default: "" },
    loc: {
        type: { type: String, default: "Point" },
        coordinates: [Number]
    },
    propertyFiles: [{ 
        // title : {type: String, default: ""},
        url : {type: String, default: ""},
        type: {type: String, default: ""} 
    }],
    isLiked: { type: Boolean, default: false },
    likeCount: { type: String, default: "0" },
    reviews: [{ type: mongoose.Schema.Types.ObjectId, ref: 'review' },],
    reviewsCount: { type: String, default: "0" },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },

});
property.index({ "loc": "2dsphere" });
mongoose.model("property", property);
module.exports = property;
// property.index({ "location": "2dsphere" });