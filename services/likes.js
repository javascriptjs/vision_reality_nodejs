const create = async(model, context) => {
    const log = context.logger.start(`services:likes:create`);
    if (!model.propertyId) {
        throw new Error("Property Id is required");
    }
    if (!model.userId) {
        throw new Error("userId is required");
    }
    let property = await db.property.find({ _id: { $eq: model.propertyId } });
    if (!property) {
        throw new Error("Property not found!!");
    }
    let user = await db.user.find({ _id: { $eq: model.userId } });
    if (!user) {
        throw new Error("User not found!!");
    }
    let userRes = await db.like.findOne({ userId: { $eq: model.userId }, propertyId: { $eq: model.propertyId } });
    if (!userRes) {
        const likeModel = {};
        likeModel.propertyId = model.propertyId;
        likeModel.userId = model.userId;
        const likeList = await new db.like(likeModel).save();
        log.end();
        return likeList
    } else {
        const likeList = await db.like.deleteOne({ userId: model.userId });
        log.end();
        return likeList
    }
};

const getFavProperties = async(id, model, context) => {
    const log = context.logger.start(`services:likes:getLikes`);
    let likes = await db.like.find({ userId: { $eq: context.user.id } }).populate('propertyId').populate('userId');;
    if (!likes) {
        throw new Error("No like");
    }
    const likeRes = likes
    log.end();
    return likeRes;
};

// const byUser = async(id, model, context) => {
//     const log = context.logger.start(`services:likes:byUser`);
//     let likes = await db.like.find({ userId: { $eq: id } });
//     if (!likes) {
//         throw new Error("No likes");
//     }
//     const likeLs = await db.property.find();
//     const getlike = [];
//     likeLs.forEach(element => {
//         for (let item of likes) {
//             if (element.id === item.productId) {
//                 getlike.push(element);
//             }
//         }
//     });
//     log.end();
//     return getlike;
// };

exports.create = create;
exports.getFavProperties = getFavProperties;
// exports.byUser = byUser;