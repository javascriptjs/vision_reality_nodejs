const encrypt = require("../permit/crypto.js");
const auth = require("../permit/auth");
const path = require("path");
const { compareSync } = require("bcrypt");
const imageUrl = require('config').get('image').url
const ObjectId = require("mongodb").ObjectID;
var nodemailer = require('nodemailer')
const crypto = require("crypto");
const { model } = require("mongoose");
const url = require('config').get('image').url


const setProperty = (model, property, context) => {
    const log = context.logger.start("services:property:set");
    
    if (model.companyName !== "string" && model.companyName !== undefined) {
        user.companyName = model.companyName;
    }
    if (model.phoneNumber !== "string" && model.phoneNumber !== undefined) {
        user.phoneNumber = model.phoneNumber;
    }
    if (model.email !== "string" && model.email !== undefined) {
        user.email = model.email;
    }
    if (model.propertyName !== "string" && model.propertyName !== undefined) {
        user.propertyName = model.propertyName;
    }
    if (model.propertyCountry !== "string" && model.propertyCountry !== undefined) {
        user.propertyCountry = model.propertyCountry;
    }
    if (model.state !== "string" && model.state !== undefined) {
        user.state = model.state;
    }
    if (model.propertyType !== "string" && model.propertyType !== undefined) {
        user.propertyType = model.propertyType;
    }
    if (model.bedrooms !== "string" && model.bedrooms !== undefined) {
        user.bedrooms = model.bedrooms;
    }
    if (model.bathrooms !== "string" && model.bathrooms !== undefined) {
        user.bathrooms = model.bathrooms;
    }
    if (model.publicTransport !== "string" && model.publicTransport !== undefined) {
        user.publicTransport = model.publicTransport;
    }
    if (model.otherInfo !== "string" && model.otherInfo !== undefined) {
        user.otherInfo = model.otherInfo;
    }
    if (model.addressLine1 !== "string" && model.addressLine1 !== undefined) {
        user.addressLine1 = model.addressLine1;
    }
    if (model.addressLine2 !== "string" && model.addressLine2 !== undefined) {
        user.addressLine2 = model.addressLine2;
    }
    if (model.zipCode !== "string" && model.zipCode !== undefined) {
        user.zipCode = model.zipCode;
    }
    if (model.type !== "string" && model.type !== undefined) {
        user.type = model.type;
    }
    if (model.status !== "string" && model.status !== undefined) {
        user.status = model.status;
    }
    if (model.lattitude !== "string" && model.lattitude !== undefined) {
        user.lattitude = model.lattitude;
    }
    if (model.longitude !== "string" && model.longitude !== undefined) {
        user.longitude = model.longitude;
    }

    log.end();
    user.save();
    return user;

};
//register user

const buildProperty = async(model, context) => {
    const { carParking,loc,coverImage,description, miles, propertyfor, closeby, price, propertyBy,lattitude, longitude, userId, companyName, phoneNumber, email, propertyName, propertyCountry, state, propertyType, bedrooms, bathrooms,publicTransport,otherInfo,addressLine1, addressLine2, zipCode, type, status } = model;
    const log = context.logger.start(`services:property:build${model}`);
    const property = await new db.property({
        carParking:carParking,
        loc:loc,
        coverImage: coverImage,
        description: description,
        miles: miles,
        closeby: closeby,
        propertyfor: propertyfor,
        lattitude: lattitude,
        propertyBy: propertyBy,
        longitude: longitude,
        userId: userId,
        companyName: companyName,
        email: email,
        propertyName: propertyName,
        phoneNumber: phoneNumber,
        state: state,
        propertyType: propertyType,
        propertyCountry: propertyCountry,
        bathrooms: bathrooms,
        addressLine1: addressLine1,
        addressLine2: addressLine2,
        bedrooms: bedrooms,
        publicTransport: publicTransport,
        zipCode: zipCode.toUpperCase(),
        otherInfo: otherInfo,
        status: status,
        type: type,
        price: price,
        createdOn: new Date(),
        updateOn: new Date()
    }).save();
    log.end();
    return property;
};

const create = async(model, context) => {
    const log = context.logger.start("services:property:create");
    model.userId = context.user.id 
    const property = buildProperty(model, context);
    log.end();
    return property;
};


// homeListing
const homeListing = async(query, context) => {
    const log = context.logger.start(`services:property:homeListing`);
    if (context.user) {
        console.log('with user')
        if(query.propertyfor === 'S'){
            let likes = await db.like.find({ userId: { $eq: context.user.id } });
            var long = query.lat;
            var lat = query.lng;
            let propertyList = await db.property.aggregate(
                [
                    { "$geoNear": {
                        "near": {
                            "type": "Point",
                            "coordinates": [Number(long),Number(lat)]
                        },
                        "distanceField": "distance",
                        "spherical": true,
                        "maxDistance": 100000000,
                        query: { propertyfor: { $eq: 'S' } ,status: { $eq: 'active' } },
                    }},
                    {
                        $lookup: {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userData"
                        },
                    },
                ])
            // let propertyList = await db.property.find({ propertyfor: { $eq: 'S' } ,status: { $eq: 'active' } }).populate('userId');
            const property = [];
            for (let element of propertyList) {
                let pId = element._id.toString();
                let likesLs = await db.like.find({ propertyId: { $eq: pId } });
                element.likeCount = likesLs.length;
                likes.forEach(like => {
                    /*converting object id to string here*/
                    let propId = like.propertyId.toString();
                    if (propId === pId) {
                        element.isLiked = true;
                    }
                });
                property.push(element);
            }
            log.end();
            return property;
        }else if(query.propertyfor === 'R'){
            let likes = await db.like.find({ userId: { $eq: context.user._id } });
            var long = query.lat;
            var lat = query.lng;
            let propertyList = await db.property.aggregate(
                [
                    { "$geoNear": {
                        "near": {
                            "type": "Point",
                            "coordinates": [Number(long),Number(lat)]
                        },
                        "distanceField": "distance",
                        "spherical": true,
                        "maxDistance": 100000,
                        query: { propertyfor: { $eq: 'R' } ,status: { $eq: 'active' } },
                    }},
                    {
                        $lookup: {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userData"
                        },
                    },
                ])
            
            // let propertyList = await db.property.find({ propertyfor: { $eq: 'R' } ,status: { $eq: 'active' } }).populate('userId');
            const property = [];
            for (let element of propertyList) {
                let pId = element._id.toString();
                let likesLs = await db.like.find({ propertyId: { $eq: pId } });
                element.likeCount = likesLs.length;
                likes.forEach(like => {
                    /*converting object id to string here*/
                    let propId = like.propertyId.toString();
                    if (propId === pId) {
                        element.isLiked = true;
                    }
                });
                property.push(element);
            }
            log.end();
            return property;
        }
    }
    else{
        console.log('without user')
        if(query.propertyfor === 'S'){
            var long = query.lat;
            var lat = query.lng;
            let allProperty = await db.property.aggregate(
                [
                    { "$geoNear": {
                        "near": {
                            "type": "Point",
                            "coordinates": [Number(long),Number(lat)]
                        },
                        "distanceField": "distance",
                        "spherical": true,
                        "maxDistance": 100000,
                        query: { propertyfor: { $eq: 'S' } ,status: { $eq: 'active' } },
                    }},
                    {
                        $lookup: {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userData"
                        },
                    },
                ])
            // let allProperty = await db.property.find({ propertyfor: { $eq: 'S' } ,status: { $eq: 'active' } }).populate('userId');
            allProperty.count = await db.property.find().count();
            log.end();
            return allProperty;
        }else if(query.propertyfor === 'R'){
            var long = query.lat;
            var lat = query.lng;
            let allProperty = await db.property.aggregate(
                [
                    { "$geoNear": {
                        "near": {
                            "type": "Point",
                            "coordinates": [Number(long),Number(lat)]
                        },
                        "distanceField": "distance",
                        "spherical": true,
                        "maxDistance": 100000,
                        query: { propertyfor: { $eq: 'R' } ,status: { $eq: 'active' } },
                    }},
                    {
                        $lookup: {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userData"
                        },
                    },
                ])
            // let allProperty = await db.property.find({ propertyfor: { $eq: 'R' } ,status: { $eq: 'active' } }).populate('userId');
            allProperty.count = await db.property.find().count();
            log.end();
            return allProperty;
        }
    }
};

// searchProperty
const searchProperty = async(query, context) => {
    if(query.searchType == 'S'){
        let allProperty = await db.property.find({
                $or: [{
                    "companyName": query.search
                },{
                    "phoneNumber": query.search
                },{
                    "email": query.search
                },{
                    "zipCode": query.search
                },{
                    "addressLine1": query.search
                },{
                    "addressLine2": query.search
                },{
                    "closeby": query.search
                }, {
                    "propertyBy": query.search
                }, {
                    "propertyName": query.search
                }],
                "propertyfor": query.propertyfor
            });
            const log = context.logger.start(`services:property:searchProperty`);
            log.end();
            return allProperty;
        }else if(query.searchType == 'F'){
            var long = query.lattitude;
            var lat = query.longitude;
            let miles = query.miles;
            let propertyfor = query.propertyfor;
            // let bedrooms = query.bedrooms;
            let minBedrooms = query.minBedrooms;
            // let minBedroomsNumber = Number(query.minBedrooms) - 1;
            // let minBedrooms = String(minBedroomsNumber);
            
            // let maxBedroomNumber = Number(query.maxBedrooms) + 1;
            // let maxBedrooms = String(maxBedroomNumber);
            let maxBedrooms = query.maxBedrooms;

            // var price = query.price
            const zipCode = query.zipCode;
            // let zipCode = query.zipCode
            let minPrice = query.minPrice
            let maxPrice = query.maxPrice;
            if (!zipCode) {
                let allProperty = await db.property.aggregate([
                    { 
                        "$match": {
                            price : { $gte :  Number(minPrice), $lte : Number(maxPrice)},
                            propertyfor: { $eq: propertyfor },
                            bedrooms : { $gte :  Number(minBedrooms), $lte : Number(maxBedrooms)},
                            status: { $eq: 'active' }
                        }
                     },
                    {
                        $lookup: {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userData"
                        },
                    },
                ])
                const log = context.logger.start(`services:property:searchProperty`);
                allProperty.count = await db.property.find().count();
                log.end();
                return allProperty;
            }else{
                let allProperty = await db.property.aggregate([
                    { "$geoNear": {
                        "near": {
                            "type": "Point",
                            "coordinates": [Number(long),Number(lat)]
                        },
                        "distanceField": "distance",
                        "spherical": true,
                        "maxDistance": Number(miles),
                        query: {
                            // zipCode: { $eq: zipCode },
                            price : { $gte :  Number(minPrice), $lte : Number(maxPrice)},
                            propertyfor: { $eq: propertyfor },
                            bedrooms : { $gte :  Number(minBedrooms), $lte : Number(maxBedrooms)},
                            status: { $eq: 'active' }
                        },
                    }},
                    {
                        $lookup: {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userData"
                        },
                    },
                ])
                const log = context.logger.start(`services:property:searchProperty`);
                allProperty.count = await db.property.find().count();
                log.end();
                return allProperty;
            }
        }
    };

// myProperty
const myProperty = async(query, context) => {
    const log = context.logger.start(`services:property:myProperty`);
    let allProperty = await db.property.find(context.id).populate('userId');
    allProperty.count = await db.property.find().count();
    log.end();
    return allProperty;
};

const propertyDetails = async(id, model, context) => {
    const log = context.logger.start(`services:property:propertyDetails`);
    let property = await db.property.findById(id).populate('userId');
    if (!property) {
        throw new Error("property not found");
    }
    let likes = await db.like.find({ propertyId: { $eq: property.id } ,userId: { $eq: context.user.id } });
    property.isLiked = likes.length
    property.likeCount = likes.length
    let reviews = await db.review.find({ propertyId: { $eq: property.id } }).populate('userId');
    property.reviews = reviews
    property.reviewsCount = reviews.length
    log.end();
    return property;
};


// const uploadPropertyFiles = async(id, files, model, context) => {
//     let bathroomFiles = files[0];
//     let washroomFiles = files[1];
//     let internalFiles = files[2];
//     let externalFiles = files[3];
//     let otherFiles = files[4];
//     const log = context.logger.start(`services:property:uploadProfilePic`);
//     let property = await db.property.findById(id);

//     if (!property) {
//         throw new Error("property not found");
//     }
//     // let baseurl = imageUrl + 'assets/property_images/';
//     if (bathroomFiles && bathroomFiles.length) {
//         for (file of bathroomFiles) {
//             const bathroomFiles = imageUrl + 'assets/property_files/' + file.filename
//             uploadfile.push({ url : bathroomFiles})
//             property.bathroomFiles = uploadfile
//         }
//     }
//     else if (washroomFiles && washroomFiles.length) {
//         for (file of washroomFiles) {
//             const washroomFiles = imageUrl + 'assets/property_files/' + file.filename
//             uploadfile.push({ url : washroomFiles})
//             property.washroomFiles = uploadfile
//         }
//     }
//     else if (internalFiles && internalFiles.length) {
//         for (file of internalFiles) {
//             const internalFiles = imageUrl + 'assets/property_files/' + file.filename
//             uploadfile.push({ url : internalFiles})
//             property.internalFiles = uploadfile
//         }
//     }
//     else if (externalFiles && externalFiles.length) {
//         for (file of externalFiles) {
//             const externalFiles = imageUrl + 'assets/property_files/' + file.filename
//             uploadfile.push({ url : externalFiles})
//             property.externalFiles = uploadfile
//         }
//     }
//     else if (otherFiles && otherFiles.length) {
//         for (file of otherFiles) {
//             const otherFiles = imageUrl + 'assets/property_files/' + file.filename
//             uploadfile.push({ url : otherFiles})
//             property.otherFiles = uploadfile
//         }
//     }
//     await property.save()
//     log.end();
//     return property
// };

const uploadPropertyFiles = async(id, files,model, context) => {
    const log = context.logger.start(`services:property:uploadProfilePic`);
    let property = await db.property.findById(id);
    if (!files) {
        throw new Error("image not found");
    }
    if (!property) {
        throw new Error("property not found!!");
    }
    if(!property.propertyFiles){
        const uploadfile = []
        for(let file of files){
            const avatar = imageUrl + 'assets/images/' + file.filename
            let fileType = file.mimetype.split('/')[0]
            uploadfile.push({ url : avatar, type: fileType})
        }
        property.propertyFiles = uploadfile
        await property.save();
        log.end();
        return property
    }else{
        for(let file of files){
            const avatar = imageUrl + 'assets/images/' + file.filename
            let fileType = file.mimetype.split('/')[0]
            property.propertyFiles =  property.propertyFiles.concat({ url : avatar, type: fileType})
        }
        await property.save();
        log.end();
        return property
    }

};
const deleteProperty = async(id, context) => {
    const log = context.logger.start(`services:property:deleteProperty`);
    if (!id) {
        throw new Error("Property Id is requried");
    }
    let property = await db.property.deleteOne({ _id: id });
    if (!property) {
        throw new Error("property not found");
    }
    property.message = 'Property Deleted Successfully';
    log.end();
    return property
};

const createDeal = async (id, model, context) => {
    const log = context.logger.start(`services:property:createDeal`);
    if (!context.user) {
        throw new Error("user not found");
    }else{
        let property = await db.property.findById(id);
        if (!property) {
            throw new Error("property not found");
        }else{
            let checkdeal = await db.deal.find({ propertyId: { $eq: id } ,userId: { $eq: context.user.id } });
            if(checkdeal && checkdeal.length){
                throw new Error("property deal is in progress");
            }else{
                const dealModel = {};
                dealModel.propertyId = id;
                dealModel.userId = context.user.id;
                const deal = await new db.deal(dealModel).save();
                log.end();
                return deal
            }
        }
    }
};

const dealAction = async (model, context) => {
    const log = context.logger.start(`services:property:dealAction`);
    if (!context.user) {
        throw new Error("user not found");
    }else{
        let checkdeal = await db.property.findOne({ _id: { $eq: model.propertyId } ,userId: { $eq: context.user.id } });
        if (!checkdeal) {
            throw new Error("Property Not Found");
        }else{
            if(model.status === 'done' ){
                if(checkdeal.status === 'active'){
                    checkdeal.status = model.status;
                    checkdeal.dealPrice = model.dealPrice;
                    checkdeal.buyerId = model.buyerId;
                    checkdeal.updatedOn = new Date();
                    await checkdeal.save();
                    log.end();
                    return checkdeal;
                }else{
                    throw new Error("Property Deal is not active");
                }
            }
            if(model.status === 'delete' ){
                let deletedeal = await db.property.findOne({ _id: { $eq: model.propertyId } ,userId: { $eq: context.user.id } });
                deletedeal.status = 'deleted';
                deletedeal.updatedOn = new Date();
                await deletedeal.save();
                log.end();
                return deletedeal;
            }
        }
    }
};

const activeDeals = async(query, context) => {
    const log = context.logger.start(`services:property:activeDeals`);
    let allProperty = await db.property.find({ status: { $eq: 'active' } ,userId: { $eq: context.user.id } }).populate('userId');
    allProperty.count = await db.property.find({ status: { $eq: 'active' } ,userId: { $eq: context.user.id } }).count();
    log.end();
    return allProperty;
};

const doneDeals = async(query, context) => {
    const log = context.logger.start(`services:property:activeDeals`);
    let allProperty = await db.property.find({ status: { $eq: 'done' } ,userId: { $eq: context.user.id } }).populate('userId');
    allProperty.count = await db.property.find({ status: { $eq: 'done' } ,userId: { $eq: context.user.id } });
    log.end();
    return allProperty;
};

const ownDeals = async(query, context) => {
    const log = context.logger.start(`services:property:activeDeals`);
    let allProperty = await db.property.find({ status: { $eq: 'done' } ,buyerId: { $eq: context.user.id } }).populate('userId');
    allProperty.count = await db.property.find({ status: { $eq: 'done' } ,buyerId: { $eq: context.user.id } });
    log.end();
    return allProperty;
};

// const imageUploader = async(model, params, context) => {
//     let log = context.logger.start('services:users:uploadImage')
//     console.log('hit in services image upload===')
//     let uploadImageOf = 'user'
//     let entity
//     let picUrl

//     entity = await db.user.findById(params.id);


//     if (!entity) {
//         throw new Error(`invalid ${uploadImageOf} Id`);
//     } else {

//         picUrl = entity.profilePic

//     }

//     let date = new Date()
//     date = date.getTime()
//     var filePath = 'assets/images/' + date
//     if (picUrl.includes(`${url}`)) {
//         picUrl = picUrl.replace(`${url}`, '');
//         try {
//             fs.unlinkSync(`${picUrl}`)
//             console.log('File unlinked!');
//         } catch (err) {
//             console.log(err)
//         }
//     }
//     if (model.files) {
//         if (Object.keys(model.files).length == 0) {
//             log.end()
//             throw new Error('No files were uploaded.');
//         }

//         filePath = filePath + model.files.file.name.replace(/\s/g, '')

//         model.files.file.mv(filePath, function(err) {
//             if (err) {
//                 log.end()
//                 throw new Error('No files were uploaded.');
//             }
//             console.log('File uploaded!');
//         });
//     }
//     var completeURl = url + filePath
//     if (uploadImageOf === 'user') {
//         entity.profilePic = completeURl
//     } else if (uploadImageOf === 'product') {
//         entity.image = completeURl
//     } else if (uploadImageOf === 'store') {
//         entity.logo = completeURl
//     }
//     await entity.save()
//     log.end()
//     return entity
// }


exports.create = create;
exports.propertyDetails = propertyDetails;
exports.searchProperty = searchProperty;
exports.myProperty = myProperty;
exports.homeListing = homeListing;
exports.deleteProperty = deleteProperty;
exports.createDeal = createDeal;
exports.dealAction = dealAction;
exports.uploadPropertyFiles = uploadPropertyFiles;
exports.activeDeals = activeDeals;
exports.doneDeals = doneDeals;
exports.ownDeals = ownDeals;