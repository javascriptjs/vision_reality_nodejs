const create = async(model, context) => {
    const log = context.logger.start(`services:reportbug:create`);
    if (!model.userId) {
        throw new Error("userId is required");
    }

    let user = await db.user.find({ _id: { $eq: model.userId } });
    if (!user) {
        throw new Error("User not found!!");
    }
        const bugModel = {};
        bugModel.userId = model.userId;
        bugModel.bug = model.bug;
        const bugList = await new db.reportbug(bugModel).save();
        log.end();
        return bugList
};

exports.create = create;