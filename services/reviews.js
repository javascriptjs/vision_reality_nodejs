const create = async(model, context) => {
    const log = context.logger.start(`services:reviews:create`);
    if (!model.propertyId) {
        throw new Error("Property Id is required");
    }
    if (!model.userId) {
        throw new Error("userId is required");
    }
    let property = await db.property.find({ _id: { $eq: model.propertyId } });
    if (!property) {
        throw new Error("Property not found!!");
    }
    let user = await db.user.find({ _id: { $eq: model.userId } });
    if (!user) {
        throw new Error("User not found!!");
    }
        const reviewModel = {};
        reviewModel.propertyId = model.propertyId;
        reviewModel.userId = model.userId;
        reviewModel.review = model.review;
        reviewModel.ratings = model.ratings;
        const reviewList = await new db.review(reviewModel).save();
        log.end();
        return reviewList
};

// const getFavProperties = async(id, model, context) => {
//     const log = context.logger.start(`services:likes:getLikes`);
//     let likes = await db.like.find({ userId: { $eq: context.user.id } }).populate('propertyId');
//     if (!likes) {
//         throw new Error("No like");
//     }
//     const likeRes = likes
//     log.end();
//     return likeRes;
// };

exports.create = create;
// exports.getFavProperties = getFavProperties;