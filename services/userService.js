const encrypt = require("../permit/crypto.js");
const auth = require("../permit/auth");
const path = require("path");
const { compareSync } = require("bcrypt");
const imageUrl = require('config').get('image').url
const ObjectId = require("mongodb").ObjectID;
var nodemailer = require('nodemailer')
const crypto = require("crypto");
const url = require('config').get('image').url


const setUser = (model, user, context) => {
    const log = context.logger.start("services:users:set");
    if (model.userName !== "string" && model.userName !== undefined) {
        user.userName = model.userName;
    }

    if (model.email !== "string" && model.email !== undefined) {
        user.email = model.email;
    }

    if (model.firstName !== "string" && model.firstName !== undefined) {
        user.firstName = model.firstName;
    }

    if (model.lastName !== "string" && model.lastName !== undefined) {
        user.lastName = model.lastName;
    }

    if (model.phoneNumber !== "string" && model.phoneNumber !== undefined) {
        user.phoneNumber = model.phoneNumber;
    }

    if (model.role !== "string" && model.role !== undefined) {
        user.role = model.role;
    }

    if (model.sex !== "string" && model.sex !== undefined) {
        user.sex = model.sex;
    }
    if (model.status !== "string" && model.status !== undefined) {
        user.status = model.status;
    }
    if (model.addressLine1 !== "string" && model.addressLine1 !== undefined) {
        user.addressLine1 = model.addressLine1;
    }
    if (model.addressLine2 !== "string" && model.addressLine2 !== undefined) {
        user.addressLine2 = model.addressLine2;
    }
    if (model.city !== "string" && model.city !== undefined) {
        user.city = model.city;
    }
    if (model.country !== "string" && model.country !== undefined) {
        user.country = model.country;
    }
    if (model.zipCode !== "string" && model.zipCode !== undefined) {
        user.zipCode = model.zipCode;
    }

    log.end();
    user.save();
    return user;

};
//register user

const buildUser = async(model, context) => {
    const { platform,socialLinkId, userName, firstName, lastName, phoneNumber, email, password, role, addressLine1, addressLine2, city, country, zipCode } = model;
    const log = context.logger.start(`services:users:build${model}`);
    const user = await new db.user({
        socialLinkId: socialLinkId,
        platform: platform,
        userName: userName,
        firstName: firstName,
        lastName: lastName,
        phoneNumber: phoneNumber,
        email: email,
        role: role,
        addressLine1: addressLine1,
        addressLine2: addressLine2,
        city: city,
        country: country,
        zipCode: zipCode,
        password: password,
        createdOn: new Date(),
        updateOn: new Date()
    }).save();
    log.end();
    return user;
};

const create = async(model, context) => {
    const log = context.logger.start("services:users:create");
    const isEmail = await db.user.findOne({ email: { $eq: model.email } });
    const userName = await db.user.findOne({ userName: { $eq: model.userName } });
    if (!model.userName) {
        //return "Email already resgister";
        throw new Error("Username is required");
    }
    if (!model.email) {
        //return "Email already resgister";
        throw new Error("Email is required");
    }
    if (isEmail) {
        //return "Email already resgister";
        throw new Error("Email already exists");
    } 
    else if(userName){
        throw new Error("Choose another username");
    } 
    else {
        model.password = encrypt.getHash(model.password, context);
        const user = buildUser(model, context);
        log.end();
        return user;
    }

};

// login 

const login = async(model, context) => {
    const log = context.logger.start("services:users:login");

    const query = {};

    // if (model.email) {
    //     query.email = model.email;
    // }
    // console.log('parameter', query)

    // let user = await db.user.findOne(query)
    let user = await db.user.findOne({
        $or: [{
          "email": model.email
        }, {
          "phone": model.email
        }, {
          "userName": model.email
        }]
    });
    if (!user) {
        log.end();
        throw new Error("user not found");
    }

    if (user.status === 'deactive') {
        return "User Is Deactive";
    }

    const isMatched = encrypt.compareHash(model.password, user.password, context);

    if (!isMatched) {
        log.end();
        throw new Error("password mismatch");
    }

    const token = auth.getToken(user.id, false, context);
    user.apiToken = token;
    user.updatedOn = new Date();
    user.save();
    log.end();
    return user;
};

const socialLink = async(model, context) => {
    const log = context.logger.start("services:users:socialLink");

    if (!model.socialLinkId) {
        throw new Error("SocialLinkId is requried");
    }
    if (!model.userName) {
        throw new Error("userName is requried");
    }
    let user = await db.user.findOne({ socialLinkId: model.socialLinkId });
    if (!user) {
        const userName = await db.user.findOne({ userName: { $eq: model.userName } });
        if(userName){
            throw new Error("Choose another username");
        }
        const createdUser = await buildUser(model, context);
        const token = auth.getToken(createdUser.id, false, context);
        createdUser.apiToken = token;
        createdUser.save();
        log.end();
        return createdUser;
    }
    const token = auth.getToken(user.id, false, context);
    user.apiToken = token;
    user.deviceToken = model.deviceToken;
    user.lat = model.lat;
    user.long = model.long;
    user.updatedOn = new Date();
    user.save();
    log.end();
    return user;
};
// createOne

const addDetails = async(model, context) => {
    const { name, email, address, contactNumber, gasType, loyalityNumber, offers } = model;
    const log = context.logger.start(`services:users:build${model}`);
    const user = await new db.addDetails({
        name: name,
        email: email,
        address: address,
        contactNumber: contactNumber,
        gasType: gasType,
        loyalityNumber: loyalityNumber,
        offers: offers,
    }).save();
    log.end();
    return addDetails;
};

// change password

const changePassword = async(id, model, context) => {
    const log = context.logger.start(`service/users/changePassword`);
    if (!id) {
        log.end();
        throw new Error("user id is required");
    }
    let user = await db.user.findById(id);
    if (!user) {
        log.end();
        throw new Error("user is not found");
    }
    const isMatched = encrypt.compareHash(
        model.oldPassword,
        user.password,
        context
    );
    if (isMatched) {
        const newPassword = encrypt.getHash(model.newPassword, context);
        user.password = newPassword;
        user.updatedOn = new Date();
        await user.save();
        log.end();
        return "Password Updated Successfully";
    } else {
        log.end();
        throw new Error("Old Password Not Match");
    }
};

// forgetPassword

const forgotPassword = async(model, context) => {
    const log = context.logger.start('service/users/forgotPassword');

    const reset = {};
    const resetPasswordToken = crypto.randomBytes(20).toString("hex");
    const resetPasswordExpires = Date.now() + 3600000; //expires in an hour

    reset.resetPasswordToken = resetPasswordToken;
    reset.resetPasswordExpires = resetPasswordExpires;

    async function storedToken() {
        await db.user.findOneAndUpdate({ email: model.email }, { $set: reset }, { new: true }).then((tkn) => {
            console.log("reset password token is stored in db", tkn);
        });
    }

    let user = await db.user.findOne({ email: model.email })

    if (!user) {
        log.end();
        throw new Error("The email address " + model.email + " is not associated with any account. Please check your email address and try again.");
    } else {
        storedToken();


        let smtpTransport = nodemailer.createTransport({
            // service: "gmail",
            host: 'smtpout.secureserver.net',
            port: 465,
            secure: true,
            auth: {
                user: 'support@visionreality.co.uk',
                pass: 'SuperAdmin@2021'
            },
            tls: {
                rejectUnauthorized: false
            }
        });


        if (user) {
            // const link = 'http://72.167.44.37:4600/resetPassword/' + resetPasswordToken;
            const OTP = Math.floor(100 + Math.random() * 9000);
            user.OTP = OTP
            user.verifyOTP = false;
            user.save();
             let mailOptions = {
                from: "support@visionreality.co.uk",
                to: `${user.email}`,
                subject: "Link To Reset Password",
                html: "Hi " +
                    "<b style='text-transform: capitalize;'>" + `${user.userName}` + "</b>" +
                    "<br>  Please Check the OTP to reset password. This is valid for one hour of receiving it.<br>" +
                    OTP ,
            };
            let mailSent = await smtpTransport.sendMail(mailOptions)
            if (mailSent) {
              console.log("Message sent: %s", mailSent.messageId);
              console.log("Preview URL: %s", nodemailer.getTestMessageUrl(mailSent));
              return user
            } else {
              log.end()
              throw new Error("Unable to send email try after sometime");
            }
        }
    }
};

const sendMail = async(model, context) => {
    const log = context.logger.start('service/users/sendMail');
    if(!model.email){
        log.end();
        throw new Error("Email id is required");
    }
    if(!model.message){
        log.end();
        throw new Error("Message is required");
    }
    let smtpTransport = nodemailer.createTransport({
        service: "gmail",
        host: 'smtp.mailtrap.io',
        port: 2525,
        secure: true,
        auth: {
            user: 'emmieandrewwork@gmail.com',
            pass: 'Notsure%1'
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    let uemail = "sankhyan.amit@gmail.com"
    let mailOptions = {
        from: "javascript.mspl@gmail.com",
        to: `${uemail}`,
        subject: "Users Queries",
        html: "Hi Admin " + "Mail From" + " " + `${model.email}` + "</b>" + "</b>" +
            "<br>  I have Queries Regarding" + " " + `${ model.message}` +".<br>",
    };
    let mailSent = await smtpTransport.sendMail(mailOptions)
    if (mailSent) {
        console.log("Message sent: %s", mailSent.messageId);
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(mailSent));
        log.end();
        return "Email Sent";
    } else {
        log.end()
        throw new Error("Unable to send email try after sometime");
    }
};

const newPassword = async(id, model, context) => {
    const log = context.logger.start(`service/users/newPassword`);
    if (!id) {
        log.end();
        throw new Error("user id is required");
    }
    let user = await db.user.findById(id);
    if (!user) {
        log.end();
        throw new Error("user is not found");
    }
        const newPassword = encrypt.getHash(model.newPassword, context);
        user.password = newPassword;
        user.updatedOn = new Date();
        await user.save();
        log.end();
        return "Password Updated Successfully";
};

const verifyOtp = async(id ,model, context) => {
    const log = context.logger.start("services:users:verifyOtp");
    let user = await db.user.findById(id);
    // if (!user) {
    //     log.end();
    //     throw new Error("user not found");
    // }

    if (user.OTP === model.OTP) {
        user.verifyOTP = true;
        user.updatedOn = new Date();
        user.save();
        log.end();
        return user;
    }else{
        log.end();
        throw new Error("Otp does not match");
    }
};

// getUsers
const getUsers = async(query, context) => {
    const log = context.logger.start(`services:users:getUsers`);
    let allUsers = await db.user.find();
    allUsers.count = await db.user.find().count();
    log.end();
    return allUsers;
};

const currentUser = async(id, model, context) => {
    const log = context.logger.start(`services:users:currentUser`);
    let user = await db.user.findById(id);
    if (!user) {
        throw new Error("user not found");
    }
    log.end();
    return user;
};

const deleteUser = async(id, context) => {
    const log = context.logger.start(`services:users:deleteUser`);
    if (!id) {
        throw new Error("userId is requried");
    }
    let user = await db.user.deleteOne({ _id: id });
    if (!user) {
        throw new Error("user not found");
    }
    user.message = 'User Deleted Successfully'
    return user
};

const update = async(id, model, context) => {
    const log = context.logger.start(`services:users:update`);
    let entity = await db.user.findById(id);
    if (!entity) {
        throw new Error("invalid user");
    }
    if (model.email) {
        let mailExists = await db.user.findOne({ email: { $eq: model.email } });
        if(mailExists){
            throw new Error("Email Exists");
        }else{
            const user = await setUser(model, entity, context);
            log.end();
            return user
        }
    }
    const user = await setUser(model, entity, context);
    log.end();
    return user
};

const uploadProfilePic = async(id, file, context) => {
    const log = context.logger.start(`services:users:uploadProfilePic`);
    let user = await db.user.findById(id);
    console.log('file==>>>', file);
    if (!file) {
        throw new Error("image not found");
    }
    if (!user) {
        throw new Error("user not found");
    }
    if (user.avatar != "" && user.avatar !== undefined) {

        let picUrl = user.avatar.replace(`${imageUrl}`, '');
        try {
            await fs.unlinkSync(`${picUrl}`)
            console.log('File unlinked!');
        } catch (err) {
            console.log(err)
        }
    }
    const avatar = imageUrl + 'assets/images/' + file.filename
    user.avatar = avatar
    await user.save();
    log.end();
    return user
};

const imageUploader = async(model, params, context) => {
    let log = context.logger.start('services:users:uploadImage')
    console.log('hit in services image upload===')
    let uploadImageOf = 'user'
    let entity
    let picUrl

    entity = await db.user.findById(params.id);


    if (!entity) {
        throw new Error(`invalid ${uploadImageOf} Id`);
    } else {

        picUrl = entity.profilePic

    }

    let date = new Date()
    date = date.getTime()
    var filePath = 'assets/images/' + date
    if (picUrl.includes(`${url}`)) {
        picUrl = picUrl.replace(`${url}`, '');
        try {
            fs.unlinkSync(`${picUrl}`)
            console.log('File unlinked!');
        } catch (err) {
            console.log(err)
        }
    }
    if (model.files) {
        if (Object.keys(model.files).length == 0) {
            log.end()
            throw new Error('No files were uploaded.');
        }

        filePath = filePath + model.files.file.name.replace(/\s/g, '')

        model.files.file.mv(filePath, function(err) {
            if (err) {
                log.end()
                throw new Error('No files were uploaded.');
            }
            console.log('File uploaded!');
        });
    }
    var completeURl = url + filePath
    if (uploadImageOf === 'user') {
        entity.profilePic = completeURl
    } else if (uploadImageOf === 'product') {
        entity.image = completeURl
    } else if (uploadImageOf === 'store') {
        entity.logo = completeURl
    }
    await entity.save()
    log.end()
    return entity
}

const searchUser = async(query, context) => {
    const log = context.logger.start(`services:users:searchUser`);
    let allusers = await db.user.find({userName: new RegExp('^'+query.search+'$', "i")})
    allusers.count = await db.user.find().count();
    log.end();
    return allusers;
};


exports.login = login;
exports.create = create;
exports.currentUser = currentUser;
exports.changePassword = changePassword;
exports.newPassword = newPassword;
exports.verifyOtp = verifyOtp;
exports.addDetails = addDetails;
exports.forgotPassword = forgotPassword;
exports.getUsers = getUsers;
exports.deleteUser = deleteUser;
exports.update = update;
exports.uploadProfilePic = uploadProfilePic;
exports.imageUploader = imageUploader;
exports.socialLink = socialLink;
exports.searchUser = searchUser;
exports.sendMail = sendMail;